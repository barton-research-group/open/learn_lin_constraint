import numpy as np
import yaml

class ParamManager:
    def __init__(self):
        # Vehicle capability matrix: (veh_type_num * cap_num)
        self.cap_eps = 1e-2
        self.veh_num_bound = 5

        self.gt_veh_cap_mat = np.array([[0,   1,   0,   0,   0,   0,   0,   2],
                                        [1,   1,   0,   0,   0,   0,   0,   0],
                                        [0,   0,   0,   1,   0,   0,   1,   0],
                                        [0,   0,   1,   0,   0,   0,   0,   5],
                                        [0,   0,   0,   0,   1,   0,   0,   0],
                                        [0,   0,   0,   0,   1,   1,   0,   0.0]])
        # Task requirement matrix: (task_num * cap_num)
        self.gt_task_req_mat = np.array([[0,   1,   0,   0,   0,   0,   0,   0],
                                        [1,   1,   0,   0,   0,   0,   0,   0],
                                        [0,   0,   0,   1,   0,   0,   1,   0],
                                        [0,   0,   1,   1,   0,   0,   2,   0],
                                        [0,   1,   1,   1,   0,   0,   1,   10],
                                        [0,   0,   0,   0,   1,   0,   0,   0],
                                        [0,   0,   0,   0,   1,   1,   0,   0.0]])
                                        # [0,   0,   0,   0,   1,   1,   0,   0.0],
                                        # [1,   1,   1,   1,   2,   2,   1,   2.0]])
        self.gt_veh_cap_mat = np.array([[1,   0,   1],
                                        [1.5,   0,   0],
                                        [1,   1,   0],
                                        [0,   1,   0],
                                        [0,   0,   1],
                                        [0,   0,   2.0]])
        self.gt_task_req_mat = np.array([[3,   0,   0],
                                        [0,   1,   0],
                                        [0,   0,   4],
                                        [1,   1,   0],
                                        [1,   0,   1],
                                        [1,   1,   1.0]])

        self.initialize()

    def initialize(self):
        self.task_num = self.gt_task_req_mat.shape[0]
        self.veh_type_num = self.gt_veh_cap_mat.shape[0]
        self.cap_num = self.gt_veh_cap_mat.shape[1]

        # self.gt_veh_cap_mat, self.gt_task_req_mat = self.rand_matrix()

        # self.veh_num = np.array([5, 5, 5, 5, 5, 5])
        self.veh_num = np.ones((self.veh_type_num), dtype=int) * self.veh_num_bound
        # self.veh_num = np.array([2, 3, 4, 5, 6, 7])

        self.veh_cap_bool = self.gt_veh_cap_mat > self.cap_eps
        self.task_req_bool = self.gt_task_req_mat > self.cap_eps

        self.task_veh_relavance = []
        self.task_veh_relavance_bool = np.zeros((self.task_num, self.veh_type_num), dtype=bool)
        for i_task in range(self.task_num):
            for i_veh_type in range(self.veh_type_num):
                self.task_veh_relavance_bool[i_task, i_veh_type] = np.logical_and(self.veh_cap_bool[i_veh_type], self.task_req_bool[i_task]).any()
            self.task_veh_relavance.append((self.task_veh_relavance_bool[i_task] > 0.5).nonzero()[0])
            # print(i_task, self.task_veh_relavance[i_task])
    
    def total_sample(self):
        total_sample_num = 0
        for i_task in range(self.task_num):
            relavant_veh_num = self.task_veh_relavance[i_task].shape[0]
            total_sample_num += (self.veh_num_bound + 1) ** relavant_veh_num
        return total_sample_num

    def rand_veh_vec(self, i_task=None):
        if not i_task:
            return np.array([np.random.randint(self.veh_num[i_veh_type]+1) for i_veh_type in range(self.veh_type_num)])
        veh_vec = np.zeros(self.veh_type_num, dtype=int)
        for i_veh_type in range(self.veh_type_num):
            if self.task_veh_relavance_bool[i_task, i_veh_type]:
                veh_vec[i_veh_type] = np.random.randint(self.veh_num[i_veh_type]+1)
        return veh_vec


    def normalize_model(self, veh_cap_mat, task_req_mat):
        # Vehicle capability matrix: (veh_type_num * cap_num)
        # Task requirement matrix: (task_num * cap_num)
        cap_num = veh_cap_mat.shape[1]
        for i_cap in range(cap_num):
            cap_norm = veh_cap_mat[:, i_cap]
            cap_norm = cap_norm[cap_norm > self.cap_eps]
            if cap_norm.shape[0] == 0:
                continue
            cap_norm = cap_norm.min()
            # print('cap_norm = ', cap_norm)
            veh_cap_mat[:, i_cap] /= cap_norm
            task_req_mat[:, i_cap] /= cap_norm

    def rand_matrix(self, task_num, veh_type_num, cap_num, task_req_num = [1,1,2,2,3,3,4,4], task_req_range = [1.0, 5.0], veh_cap_range = [1.0, 3.0], veh_has_cap_num = 2):
        # veh_cap_range = [1.0, 3.0]
        veh_cap_bool = np.zeros((veh_type_num, cap_num), dtype=bool)
        for i_veh_type in range(veh_type_num):
            i_cap = np.random.permutation(cap_num)[:veh_has_cap_num]
            veh_cap_bool[i_veh_type, i_cap] = True
        for i_cap in range(cap_num):
            if not veh_cap_bool[:, i_cap].any():
                i_veh_type = np.random.randint(veh_type_num)
                veh_cap_bool[i_veh_type, i_cap] = True
        veh_cap_mat = veh_cap_bool * (np.random.rand(veh_type_num, cap_num) * (veh_cap_range[1] - veh_cap_range[0]) + veh_cap_range[0])

        # task_req_num = [1,1,2,2,3,3,4,cap_num]
        # task_req_num = [1,1,2,2,3,3,4,4]
        # task_req_range = [1.0, 5.0]
        task_req_bool = np.zeros((task_num, cap_num), dtype=bool)

        for i_task in range(task_num):
            i_cap = np.random.permutation(cap_num)[:task_req_num[i_task]]
            task_req_bool[i_task, i_cap] = True

        task_req_mat = task_req_bool * (np.random.rand(task_num, cap_num) * (task_req_range[1] - task_req_range[0]) + task_req_range[0])

        self.normalize_model(veh_cap_mat, task_req_mat)

        # print('veh_cap_bool = ', veh_cap_bool)
        # print('veh_cap_mat = ', veh_cap_mat)
        # print('task_req_mat = ', task_req_mat)
        self.gt_veh_cap_mat = veh_cap_mat
        self.gt_task_req_mat = task_req_mat
        self.initialize()
        return veh_cap_mat, task_req_mat

    def to_dict(self):
        decimal_set = 4
        temp_veh_cap_mat = np.round(self.gt_veh_cap_mat, decimals=decimal_set)
        temp_task_req_mat = np.round(self.gt_task_req_mat, decimals=decimal_set)

        temp_param = {}
        temp_param['veh_cap_mat'] = temp_veh_cap_mat.tolist()
        temp_param['task_req_mat'] = temp_task_req_mat.tolist()
        temp_param['veh_num_bound'] = self.veh_num_bound
        return temp_param

    def save_to_file(self, file_name):
        temp_param = self.to_dict()
        with open(file_name, 'w') as outfile:
            yaml.safe_dump(temp_param, outfile, default_flow_style=None, width=float("inf"))
        return True

    def read_from_file(self, file_name):
        with open(file_name, 'r') as stream:
            try:
                yaml_load = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
                return False
        self.gt_veh_cap_mat = np.array(yaml_load['veh_cap_mat'])
        self.gt_task_req_mat = np.array(yaml_load['task_req_mat'])
        self.veh_num_bound = yaml_load['veh_num_bound']
        self.initialize()
        return True

    def add_noise(self, noise_rate):
        for i_task in range(self.task_num):
            for i_cap in range(self.cap_num):
                if np.random.rand() <= noise_rate:
                    if self.gt_task_req_mat[i_task, i_cap] > self.cap_eps:
                        pass
                        # self.gt_task_req_mat[i_task, i_cap] = 0.0
                    else:
                        self.gt_task_req_mat[i_task, i_cap] = 1.0
        for i_veh_type in range(self.veh_type_num):
            for i_cap in range(self.cap_num):
                if np.random.rand() <= noise_rate:
                    if self.gt_veh_cap_mat[i_veh_type, i_cap] > self.cap_eps:
                        pass
                        # self.gt_veh_cap_mat[i_veh_type, i_cap] = 0.0
                    else:
                        self.gt_veh_cap_mat[i_veh_type, i_cap] = 1.0
        self.initialize()
