import numpy as np
from ParamManager import ParamManager
from ConstrLearner import ConstrLearner

veh_sample1 = np.array([[1, 0, 0, 0, 0],
    [0, 1, 0, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 1, 0],
    [0, 0, 0, 0, 1],
    [2, 0, 0, 0, 0],
    [0, 2, 0, 0, 0],
    [0, 0, 2, 0, 0],
    [0, 0, 0, 2, 0],
    [0, 0, 0, 0, 2],
    [1, 1, 0, 0, 0],
    [1, 0, 1, 0, 0],
    [1, 0, 0, 1, 0],
    [1, 0, 0, 0, 1],
    [0, 1, 1, 0, 0],
    [0, 1, 0, 1, 0],
    [0, 1, 0, 0, 1],
    [0, 0, 1, 1, 0],
    [0, 0, 1, 0, 1],
    [0, 0, 0, 1, 1]])

veh_sample2 = np.array([[0, 0, 0, 1, 0],
                [0, 0, 0, 0, 1],
                [0, 0, 0, 2, 0],
                [0, 0, 0, 0, 2],
                [0, 0, 0, 1, 1],
                [0, 0, 0, 3, 0],
                [0, 0, 0, 0, 3],
                [0, 0, 0, 2, 1],
                [0, 0, 0, 1, 2],
                [0, 0, 0, 4, 0],
                [0, 0, 0, 0, 4],
                [0, 0, 0, 3, 1],
                [0, 0, 0, 1, 3],
                [0, 0, 0, 2, 2]]) # 14

veh_sample3 = np.array([[0, 0, 0, 0, 1],
                [0, 0, 0, 1, 1],
                [0, 0, 0, 2, 1],
                [0, 0, 0, 3, 1],
                [0, 0, 0, 0, 2],
                [0, 0, 0, 1, 2],
                [0, 0, 0, 2, 2],
                [0, 0, 0, 0, 3],
                [0, 0, 0, 1, 3],
                [0, 0, 0, 0, 4]]) # 10

veh_sample4 = np.array([[0, 0, 0, 0, 1],
                [0, 0, 0, 0, 2],
                [0, 0, 0, 0, 3],
                [0, 0, 0, 0, 4]]) # 4

veh_sample5 = np.array([[0, 0, 0, 1, 0],
                [0, 0, 0, 0, 1],
                [0, 0, 0, 2, 0],
                [0, 0, 0, 0, 2],
                [1, 0, 0, 1, 0],
                [1, 0, 0, 0, 1],
                [0, 1, 0, 1, 0],
                [0, 1, 0, 0, 1],
                [0, 0, 1, 1, 0],
                [0, 0, 1, 0, 1],
                [0, 0, 0, 1, 1],
                [0, 0, 0, 3, 0],
                [0, 0, 0, 0, 3],
                [1, 0, 0, 1, 1],
                [0, 1, 0, 1, 1],
                [0, 0, 1, 1, 1],
                [0, 1, 1, 0, 1],
                [0, 1, 1, 1, 0],
                [1, 1, 0, 0, 1],
                [1, 1, 0, 1, 0],
                [1, 1, 1, 1, 0],
                [0, 1, 1, 1, 1],
                [1, 0, 1, 1, 1],
                [1, 1, 0, 1, 1],
                [1, 1, 1, 0, 1]]) # 25

task_time1 = np.array([234.394, 185.496, 244.895, 229.396, 182.795, 156.390, 105.889, 138.239, 151.990, 85.487, 136.991, 143.695, 168.390, 147.814, 128.459, 145.389, 93.491, 136.491, 148.887, 145.882])
task_time2 = np.array([128.195, 211.292, 98.359, 133.860, 121.447, 83.358, 123.798, 99.969, 86.351, 44.363, 81.030, 49.712, 59.325, 58.907])
task_time3 = np.array([252.095, 121.425, 112.181, 113.384, 145.155, 77.605, 66.378, 117.880, 80.523, 83.281])
task_time4 = np.array([203.493, 160.562, 126.063, 70.354])
task_time5 = np.array([108.894, 111.291, 73.482, 88.487, 66.489, 84.388, 62.879, 73.192, 69.289, 95.286, 87.885, 58.740, 72.191, 64.301, 66.978, 62.406, 85.776, 77.249, 74.381, 58.468, 58.768, 44.315, 62.331, 56.264, 73.876])

task_time_thres1 = 190.0
task_time_thres2 = 110.0
task_time_thres3 = 130.0
task_time_thres4 = 140.0
task_time_thres5 = 90.0

performance_sample1 = task_time1 < task_time_thres1
performance_sample2 = task_time2 < task_time_thres2
performance_sample3 = task_time3 < task_time_thres3
performance_sample4 = task_time4 < task_time_thres4
performance_sample5 = task_time5 < task_time_thres5

# Generate samples
veh_sample_all = [veh_sample1, veh_sample2, veh_sample3, veh_sample4, veh_sample5]
performance_sample_all = [performance_sample1, performance_sample2, performance_sample3, performance_sample4, performance_sample5]

param_manager = ParamManager()

param_manager.veh_num_bound = 4

flag_more_feature = True
if flag_more_feature:
    param_manager.gt_veh_cap_mat = np.array([[1.0, 0, 0, 1.0, 0],
                                    [1.5, 0, 0, 1.5, 0],
                                    [1.0, 0, 0, 1.0, 0],
                                    [1.0, 2, 0, 1.0, 2],
                                    [1.5, 1, 1, 1.5, 1]])
    param_manager.gt_task_req_mat = np.array([[2, 0, 0, 0, 0],
                                            [0, 2, 0, 0, 0],
                                            [0, 2, 2, 0, 0],
                                            [0, 0, 2, 0, 0],
                                            [0, 0, 0.0, 1.0, 1.0]])
else:
    param_manager.gt_veh_cap_mat = np.array([[1.0, 0, 0],
                                    [1.5, 0, 0],
                                    [1.0, 0, 0],
                                    [1.0, 2, 0],
                                    [1.5, 1, 1]])
    param_manager.gt_task_req_mat = np.array([[2, 0, 0],
                                            [0, 2, 0],
                                            [0, 2, 2],
                                            [0, 0, 2],
                                            [1, 1, 0.0]])

# param_manager.gt_veh_cap_mat = (np.random.rand(5,10) > 0.3).astype(np.float64)
# param_manager.gt_task_req_mat = (np.random.rand(5,10) > 0.3).astype(np.float64)

param_manager.initialize()

constr_learner = ConstrLearner(param_manager)
constr_learner.alpha_learn = 500.0
constr_learner.alpha_a = 0.25
constr_learner.alpha_b = 1.0

for i_task in range(param_manager.task_num):
    print('veh_sample = ', i_task, veh_sample_all[i_task].shape)
    print('performance_sample = ', i_task, performance_sample_all[i_task].shape)

veh_cap_mat, task_req_mat, veh_cap_mat_ori, task_req_mat_ori = constr_learner.optimize(veh_sample_all, performance_sample_all)

for i_task in range(param_manager.task_num):
    label_pred = veh_sample_all[i_task].dot(veh_cap_mat) - task_req_mat[i_task] #  (sample_num, veh_num) * (veh_num, cap_num) = (sample_num, cap_num)
    label_pred = (label_pred >= -0.001).all(axis=1) + 0.0
    label_truth = (performance_sample_all[i_task]) + 0.0
    error = np.abs(label_truth - label_pred).sum()
    sample_num = label_pred.shape[0]
    print(i_task, error, '/', sample_num)
    print('pred', label_pred[:20])
    print('true', label_truth[:20])

print("A = \n", veh_cap_mat)
print("b = \n", task_req_mat)
