addpath('lib/YAMLMatlab_0.4.3/');

clear;
clc;

flag_task = 5;


switch flag_task
    case 1
        task_folder = 'explore/';
        block_type_array = [0, 0];
        robot_type_array_list = [[1, 0, 0, 0, 0]
            [0, 1, 0, 0, 0]
            [0, 0, 1, 0, 0]
            [0, 0, 0, 1, 0]
            [0, 0, 0, 0, 1]
            [2, 0, 0, 0, 0]
            [0, 2, 0, 0, 0]
            [0, 0, 2, 0, 0]
            [0, 0, 0, 2, 0]
            [0, 0, 0, 0, 2]
            [1, 1, 0, 0, 0]
            [1, 0, 1, 0, 0]
            [1, 0, 0, 1, 0]
            [1, 0, 0, 0, 1]
            [0, 1, 1, 0, 0]
            [0, 1, 0, 1, 0]
            [0, 1, 0, 0, 1]
            [0, 0, 1, 1, 0]
            [0, 0, 1, 0, 1]
            [0, 0, 0, 1, 1]];
    case 2
        task_folder = 'picklight/';
        block_type_array = [4, 0];
        robot_type_array_list = [[0, 0, 0, 1, 0]
            [0, 0, 0, 0, 1]
            [0, 0, 0, 2, 0]
            [0, 0, 0, 0, 2]
            [0, 0, 0, 1, 1]
            [0, 0, 0, 3, 0]
            [0, 0, 0, 0, 3]
            [0, 0, 0, 2, 1]
            [0, 0, 0, 1, 2]
            [0, 0, 0, 4, 0]
            [0, 0, 0, 0, 4]
            [0, 0, 0, 3, 1]
            [0, 0, 0, 1, 3]
            [0, 0, 0, 2, 2]]; % 14
    case 3
        task_folder = 'pickboth/';
        block_type_array = [2, 2];
        robot_type_array_list = [[0, 0, 0, 0, 1]
            [0, 0, 0, 1, 1]
            [0, 0, 0, 2, 1]
            [0, 0, 0, 3, 1]
            [0, 0, 0, 0, 2]
            [0, 0, 0, 1, 2]
            [0, 0, 0, 2, 2]
            [0, 0, 0, 0, 3]
            [0, 0, 0, 1, 3]
            [0, 0, 0, 0, 4]]; % 10
        
    case 4
        task_folder = 'pickheavy/';
        block_type_array = [0, 4];
        robot_type_array_list = [[0, 0, 0, 0, 1]
            [0, 0, 0, 0, 2]
            [0, 0, 0, 0, 3]
            [0, 0, 0, 0, 4]]; % 4
    case 5
        task_folder = 'find/';
        block_type_array = [1, 0];
        robot_type_array_list = [[0, 0, 0, 1, 0]
            [0, 0, 0, 0, 1]
            [0, 0, 0, 2, 0]
            [0, 0, 0, 0, 2]
            [1, 0, 0, 1, 0]
            [1, 0, 0, 0, 1]
            [0, 1, 0, 1, 0]
            [0, 1, 0, 0, 1]
            [0, 0, 1, 1, 0]
            [0, 0, 1, 0, 1]
            [0, 0, 0, 1, 1]
            [0, 0, 0, 3, 0]
            [0, 0, 0, 0, 3]
            [1, 0, 0, 1, 1]
            [0, 1, 0, 1, 1]
            [0, 0, 1, 1, 1]
            [0, 1, 1, 0, 1]
            [0, 1, 1, 1, 0]
            [1, 1, 0, 0, 1]
            [1, 1, 0, 1, 0]
            [1, 1, 1, 1, 0]
            [0, 1, 1, 1, 1]
            [1, 0, 1, 1, 1]
            [1, 1, 0, 1, 1]
            [1, 1, 1, 0, 1]]; % 25
        
    otherwise
        disp('Wrong flag.')
end


backup_folder = './data/learn_case2/';

backup_launch_folder = 'launch/';
backup_case_folder = 'case/';
backup_result_folder = 'result/';

repeat_num = 5;
index_select = 4;

case_num = size(robot_type_array_list, 1);
result_table = zeros(case_num, repeat_num+1);
sort_table = zeros(case_num, repeat_num);

for index_case = 1:case_num
    one_row = zeros(1, repeat_num);
    for index_repeat = 1:repeat_num
        robot_type_array = robot_type_array_list(index_case, :);
        
        robot_type_num = length(robot_type_array);
        block_type_num = length(block_type_array);
        
        case_name = 'r';
        for i_robot_type = 1:robot_type_num
            case_name = [case_name, num2str(robot_type_array(i_robot_type))];
        end
        case_name = [case_name, '_b'];
        for i_block_type = 1:block_type_num
            case_name = [case_name, num2str(block_type_array(i_block_type))];
        end
        
        str_repeat = ['repeat', num2str(index_repeat-1)];
        backup_launch_file = [backup_folder, task_folder, backup_launch_folder, case_name, str_repeat, '.launch'];
        backup_case_file = [backup_folder, task_folder, backup_case_folder, case_name, str_repeat, '.yaml'];
        backup_result_file = [backup_folder, task_folder, backup_result_folder, case_name, str_repeat, '.yaml'];
        disp(backup_result_file);
        
        resultFile = ReadYaml(backup_result_file);
        
        if flag_task == 5
            result_task_duration = resultFile.task0.task_duration + resultFile.task1.task_duration;
            result_action_duration = [];
        else
            result_task_duration = resultFile.task0.task_duration;
            result_action_duration = cell2mat(resultFile.task0.action_duration);
        end
        one_row(index_repeat) = result_task_duration;
    end
    [one_row_sort, index_sort] = sort(one_row);
    sort_table(index_case, 1:repeat_num) = index_sort;
    result_table(index_case, 2:repeat_num+1) = one_row;
    result_table(index_case, 1) = one_row_sort(index_select);
end

task_duration = result_table(:,1);
print_numpy(task_duration);
task_statistics = [min(task_duration), max(task_duration), median(task_duration), mean(task_duration), std(task_duration)];

