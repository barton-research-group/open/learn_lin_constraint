# Learning Linear Contraints as a Binary Classifier

The program learns a set of linear constraints A y ≥ b to approximate a binary function f(y) = 0 or 1.

# Real World Application: Learning Task Requirements and Agent Capabilities for Multi-agent Task Allocation

This repository implements a learning model to estimate an agent capability and task requirement model for multi-agent task allocation.

An example agent capability and task requirement model is shown as follows. Each agent type has a set of capabilities and each task requires an agent team with enough total capability to complete.

With a set of team configurations (the number of agent from each type concatenated as a vector) and the corresponding task performances (a scalar value) as the training data,  the values in table below can be learned. For more details, refer to [1].

<table>

<tr><td>

| Agent Type | E1 | M1 | M2 |
| ---------- | -- | -- | -- |
| Bot 1      | 1  | 0  | 1  |
| Bot 2      | 1.5| 0  | 0  |
| Bot 3      | 1  | 1  | 0  |
| Bot 4      | 0  | 1  | 0  |
| Bot 5      | 0  | 0  | 1  |
| Bot 6      | 0  | 0  | 2  |

</td><td>

</td><td>

| Task                             | E1 | M1 | M2 |
| -------------------------------- | -- | -- | -- |
| Explore a region                 | 3  | 0  | 0  |
| Pick light blocks                | 0  | 1  | 0  |
| Pick heavy blocks                | 0  | 0  | 4  |
| Find light blocks and pick       | 1  | 1  | 0  |
| Find heavy blocks and pick       | 1  | 0  | 1  |
| Find light/heavy blocks and pick | 1  | 1  | 1  |

</td></tr> 
<tr>
E1: exploration, M1: the manipulation capability related to light blocks, M2: the manipulation capability related to heavy blocks
</tr>

</table>



## Run a small example
Run a small example where the assumed ground truth capabilities and task requirements are listed above. The result is printerd in the console.
```bash
python3 learn_ab_example.py
```

## Run the learning model on a real multi-agent exploration and manipulation task
Since this is a real-world example, there is no ground truth capability or requirement values (i.e., ground truth A and b). But we show the prediction error rate.
```bash
python3 learn_team.py
```

## Run a range of computational examples
Run a range of randomly generated test scenarios to evaluate the accuracy and scalability of the model. The test scenarios have different numbers of tasks, capability types, and training samples.

First, run the following command to randomly generate the test scenarios.
```bash
python3 gen_test_case.py
```

Then, run the learning model on all the test scenarios. By default, the learning model runs on a randomly selected 200 training samples on each test. Change the `flag_random` to `False` to see the training result performed on the whole set.
```bash
python3 loop_test_case.py
```

Finally, visualize the computational time and testing accuracy as box plots.
```bash
python3 plot_box.py
```

## Attribution
YAMLMatlab 0.4.3: from https://code.google.com/archive/p/yamlmatlab/downloads

M3500 data set: from https://lucacarlone.mit.edu/datasets/ and https://github.com/RainerKuemmerle/g2o/wiki/File-Format-SLAM-2D

## References
[1] B. Fu, W. Smith, D. Rizzo, M. Castanier, M. Ghaffari, and K. Barton, "Learning Task Requirements and Agent Capabilities for Multi-agent Task Allocation," arXiv preprint arXiv:2211.03286, 2022. \[[PDF](https://arxiv.org/pdf/2211.03286.pdf)\] \[[Video](https://youtu.be/r_7ZsEk6axU)\]
