function print_numpy(mat)

[n_row, n_col] = size(mat);

if n_row == 1 || n_col == 1
    n_length = length(mat);
    fprintf('np.array([');
    for i = 1:n_length
        fprintf('%.3f', mat(i));
        if i < n_length
            fprintf(', ');
        end
    end
    fprintf('])\n');
end

end
