import numpy as np
from ParamManager import ParamManager
from SimWrapper import SimWrapper
from ConstrLearner import ConstrLearner

param_manager = ParamManager()
sim_wrapper = SimWrapper(param_manager)
constr_learner = ConstrLearner(param_manager)

veh_sample_all = []
performance_sample_all = []

for i_task in range(param_manager.task_num):
    veh_arange_list = []
    for i_veh_type in param_manager.task_veh_relavance[i_task]:
        veh_arange_list.append(np.arange(param_manager.veh_num[i_veh_type]+1))

    veh_sample_list = np.meshgrid(*veh_arange_list)
    veh_sample_array = np.array(veh_sample_list).reshape(len(veh_sample_list), -1).T
    sample_num = veh_sample_array.shape[0]
    veh_sample = np.zeros((sample_num, param_manager.veh_type_num), dtype=np.int64)
    veh_sample[:, param_manager.task_veh_relavance[i_task]] = veh_sample_array

    performance_sample_list = []
    performance_sample = np.zeros(sample_num, dtype=np.float64)
    for i_sample in range(sample_num):
        performance_sample[i_sample] = sim_wrapper.task_performance(i_task, veh_sample[i_sample])
    # performance_sample[1] = 0.0
    print('veh_sample = ', veh_sample.shape)
    print('performance_sample = ', performance_sample.shape)

    veh_sample_all.append(veh_sample)
    performance_sample_all.append(performance_sample)

veh_cap_mat, task_req_mat, veh_cap_mat_ori, task_req_mat_ori = constr_learner.optimize(veh_sample_all, performance_sample_all)

for i_task in range(param_manager.task_num):
    label_pred = veh_sample_all[i_task].dot(veh_cap_mat) - task_req_mat[i_task] #  (sample_num, veh_num) * (veh_num, cap_num) = (sample_num, cap_num)
    label_pred = (label_pred >= 0.0).all(axis=1) + 0.0
    label_truth = (performance_sample_all[i_task] > 0.5) + 0.0
    error = np.abs(label_truth - label_pred).sum()
    sample_num = label_pred.shape[0]
    print(i_task, error, '/', sample_num)
    print('pred', label_pred[:20])
    print('true', label_truth[:20])

print("A = \n", veh_cap_mat)
print("b = \n", task_req_mat)

print("diff A = \n", veh_cap_mat - param_manager.gt_veh_cap_mat)
print("diff b = \n", task_req_mat - param_manager.gt_task_req_mat)

print("gt_veh_cap_mat = \n", param_manager.gt_veh_cap_mat)
print("gt_task_req_mat = \n", param_manager.gt_task_req_mat)
