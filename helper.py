import numpy as np
import yaml
from ParamManager import ParamManager

def gen_samples(param_file = None, sample_file = 'temp_yaml.yaml'):
    param_manager = ParamManager()
    if param_file:
        param_manager.read_from_file(param_file)
        print('param_file = ', param_file)
        print('param_manager.task_num = ', param_manager.task_num)
    max_draw_num = 200
    sample_num_thres = max_draw_num
    sample_dict = {}

    for i_task  in range(param_manager.task_num):
        sample_num = 1
        for i_veh_type in param_manager.task_veh_relavance[i_task]:
            sample_num *= param_manager.veh_num[i_veh_type]+1
        print('task:', i_task, 'sample_num = ', sample_num)
        if sample_num < sample_num_thres:
            continue
        
        X_observed = np.empty((max_draw_num, param_manager.veh_type_num), dtype=int)
        for i_iter in range(max_draw_num):
            X_observed[i_iter] = param_manager.rand_veh_vec(i_task=i_task)
        sample_dict['X_observed' + str(i_task)] = X_observed.tolist()

    save_dict(sample_file, sample_dict)

def evaluate_result(veh_sample_all, performance_sample_all, performance_thres, label_pred_all):
    task_num = len(veh_sample_all)
    for i_task in range(task_num):
        label_pred = label_pred_all[i_task]
        label_truth = performance_sample_all[i_task] >= performance_thres

        label_pred_not = np.logical_not(label_pred)
        label_truth_not = np.logical_not(label_truth)
        # true_as_true = np.logical_and(label_truth, label_pred).sum()
        # true_as_false = np.logical_and(label_truth,  label_pred == False).sum()
        # false_as_true = np.logical_and(label_truth == False, label_pred).sum()
        # false_as_false = np.logical_and(label_truth == False, label_pred == False).sum()
        true_as_true = np.logical_and(label_truth, label_pred).sum()
        true_as_false = np.logical_and(label_truth, label_pred_not).sum()
        false_as_true = np.logical_and(label_truth_not, label_pred).sum()
        false_as_false = np.logical_and(label_truth_not, label_pred_not).sum()

        sum1 = true_as_true + true_as_false + false_as_true + false_as_false
        sum2 = true_as_true + false_as_false

        error = np.abs(label_truth.astype(int) - label_pred.astype(int)).sum()
        sample_num = label_pred.shape[0]

        temp_result = [i_task, error, sample_num, true_as_true, true_as_false, false_as_true, false_as_false]
        if i_task == 0:
            result_mat = np.empty((task_num, len(temp_result)), dtype=int)
        result_mat[i_task] = temp_result
        print("Task : %d, error: %d/%d, (%d, %d, %d, %d)" % (i_task, error, sample_num, true_as_true, true_as_false, false_as_true, false_as_false))
    result_table = result_mat.tolist()
    return result_table

def predict(veh_sample_all, veh_cap_mat, task_req_mat):
    label_pred_all = []
    for i_task in range(len(veh_sample_all)):
        label_pred = veh_sample_all[i_task].dot(veh_cap_mat) - task_req_mat[i_task] #  (sample_num, veh_num) * (veh_num, cap_num) = (sample_num, cap_num)
        label_pred = (label_pred >= -0.0001).all(axis=1)
        label_pred_all.append(label_pred)
    return label_pred_all

def save_dict(file_name, result_dict):
    with open(file_name, 'w') as outfile:
        yaml.safe_dump(result_dict, outfile, default_flow_style=None, width=float("inf"))
    return True

def read_dict(file_name):
    with open(file_name, 'r') as stream:
        try:
            yaml_load = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
            return None
    return yaml_load
