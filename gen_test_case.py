from ParamManager import ParamManager
import os

param_manager = ParamManager()

file_name = 'test_case.yaml'
param_manager.save_to_file(file_name)


test_folder = 'test_case/temp_case/'
task_req_range = [1.0, 5.0]
veh_cap_range = [1.0, 3.0]
rand_num = 10

if not os.path.exists(test_folder):
    os.makedirs(test_folder)

# Case 0
# task_num = 8
# veh_type_num = 6
# cap_num = 8
# task_req_num = [2,2,3,3,4,4,8,8]
# label_name = '_more'


# Case 1-1
task_num = 8
veh_type_num = 6
cap_num = 8
veh_has_cap_num = 2
task_req_num = [1,1,2,2,3,3,4,4]
sample_num_bound = [10000, 15000]
label_name = '_sparse'
case_folder = 'm' + str(task_num) + '_v' + str(veh_type_num) + '_c' + str(cap_num) + label_name + '/'
mean_sample_num = 0
print('T', task_num, 'V', veh_type_num, 'C', cap_num, ',', veh_has_cap_num, 'S', sample_num_bound, ': ', end = '')
if not os.path.exists(test_folder + case_folder):
    os.makedirs(test_folder + case_folder)
for i_rand in range(rand_num):
    file_name = test_folder + case_folder + 'case' + str(i_rand) + '.yaml'
    while True:
        param_manager.rand_matrix(task_num, veh_type_num, cap_num, task_req_num = task_req_num, task_req_range = task_req_range, veh_cap_range = veh_cap_range, veh_has_cap_num = veh_has_cap_num)
        total_sample_num = param_manager.total_sample()
        if sample_num_bound[0] < total_sample_num < sample_num_bound[1]:
            break
    param_manager.save_to_file(file_name)
    print(param_manager.total_sample(), end='\t')
    mean_sample_num += total_sample_num
mean_sample_num /= rand_num
print('mean', mean_sample_num)

task_num = 8
veh_type_num = 6
cap_num = 8
veh_has_cap_num = 2
task_req_num = [1,1,2,2,3,3,4,4]
sample_num_bound = [40000, 50000]
label_name = ''
case_folder = 'm' + str(task_num) + '_v' + str(veh_type_num) + '_c' + str(cap_num) + label_name + '/'
mean_sample_num = 0
print('T', task_num, 'V', veh_type_num, 'C', cap_num, ',', veh_has_cap_num, 'S', sample_num_bound, ': ', end = '')
if not os.path.exists(test_folder + case_folder):
    os.makedirs(test_folder + case_folder)
for i_rand in range(rand_num):
    file_name = test_folder + case_folder + 'case' + str(i_rand) + '.yaml'
    while True:
        param_manager.rand_matrix(task_num, veh_type_num, cap_num, task_req_num = task_req_num, task_req_range = task_req_range, veh_cap_range = veh_cap_range, veh_has_cap_num = veh_has_cap_num)
        total_sample_num = param_manager.total_sample()
        if sample_num_bound[0] < total_sample_num < sample_num_bound[1]:
            break
    param_manager.save_to_file(file_name)
    print(param_manager.total_sample(), end='\t')
    mean_sample_num += total_sample_num
mean_sample_num /= rand_num
print('mean', mean_sample_num)

# Case 1-2
task_num = 8
veh_type_num = 6
cap_num = 16
veh_has_cap_num = 4
task_req_num = [1,1,2,2,3,3,4,4]
sample_num_bound = [40000, 50000]
label_name = ''
case_folder = 'm' + str(task_num) + '_v' + str(veh_type_num) + '_c' + str(cap_num) + label_name + '/'
mean_sample_num = 0
print('T', task_num, 'V', veh_type_num, 'C', cap_num, ',', veh_has_cap_num, 'S', sample_num_bound, ': ', end = '')
if not os.path.exists(test_folder + case_folder):
    os.makedirs(test_folder + case_folder)
for i_rand in range(rand_num):
    file_name = test_folder + case_folder + 'case' + str(i_rand) + '.yaml'
    while True:
        param_manager.rand_matrix(task_num, veh_type_num, cap_num, task_req_num = task_req_num, task_req_range = task_req_range, veh_cap_range = veh_cap_range, veh_has_cap_num = veh_has_cap_num)
        total_sample_num = param_manager.total_sample()
        if sample_num_bound[0] < total_sample_num < sample_num_bound[1]:
            break
    param_manager.save_to_file(file_name)
    print(param_manager.total_sample(), end='\t')
    mean_sample_num += total_sample_num
mean_sample_num /= rand_num
print('mean', mean_sample_num)

# Case 1-3
task_num = 8
veh_type_num = 6
cap_num = 32
veh_has_cap_num = 8
task_req_num = [1,1,2,2,3,3,4,4]
sample_num_bound = [40000, 50000]
label_name = ''
case_folder = 'm' + str(task_num) + '_v' + str(veh_type_num) + '_c' + str(cap_num) + label_name + '/'
mean_sample_num = 0
print('T', task_num, 'V', veh_type_num, 'C', cap_num, ',', veh_has_cap_num, 'S', sample_num_bound, ': ', end = '')
if not os.path.exists(test_folder + case_folder):
    os.makedirs(test_folder + case_folder)
for i_rand in range(rand_num):
    file_name = test_folder + case_folder + 'case' + str(i_rand) + '.yaml'
    while True:
        param_manager.rand_matrix(task_num, veh_type_num, cap_num, task_req_num = task_req_num, task_req_range = task_req_range, veh_cap_range = veh_cap_range, veh_has_cap_num = veh_has_cap_num)
        total_sample_num = param_manager.total_sample()
        if sample_num_bound[0] < total_sample_num < sample_num_bound[1]:
            break
    param_manager.save_to_file(file_name)
    print(param_manager.total_sample(), end='\t')
    mean_sample_num += total_sample_num
mean_sample_num /= rand_num
print('mean', mean_sample_num)


# Case 2-1
task_num = 20
veh_type_num = 6
cap_num = 8
veh_has_cap_num = 2
task_req_num = [1,1,2,2,3,3,4,4,5,5,  1,1,2,2,3,3,4,4,5,5]
sample_num_bound = [140000, 160000]
label_name = ''
case_folder = 'm' + str(task_num) + '_v' + str(veh_type_num) + '_c' + str(cap_num) + label_name + '/'
mean_sample_num = 0
print('T', task_num, 'V', veh_type_num, 'C', cap_num, ',', veh_has_cap_num, 'S', sample_num_bound, ': ', end = '')
if not os.path.exists(test_folder + case_folder):
    os.makedirs(test_folder + case_folder)
for i_rand in range(rand_num):
    file_name = test_folder + case_folder + 'case' + str(i_rand) + '.yaml'
    while True:
        param_manager.rand_matrix(task_num, veh_type_num, cap_num, task_req_num = task_req_num, task_req_range = task_req_range, veh_cap_range = veh_cap_range, veh_has_cap_num = veh_has_cap_num)
        total_sample_num = param_manager.total_sample()
        if sample_num_bound[0] < total_sample_num < sample_num_bound[1]:
            break
    param_manager.save_to_file(file_name)
    print(param_manager.total_sample(), end='\t')
    mean_sample_num += total_sample_num
mean_sample_num /= rand_num
print('mean', mean_sample_num)

# Case 2-2
task_num = 20
veh_type_num = 6
cap_num = 16
veh_has_cap_num = 4
task_req_num = [1,1,2,2,3,3,4,4,5,5,  1,1,2,2,3,3,4,4,5,5]
sample_num_bound = [140000, 160000]
label_name = ''
case_folder = 'm' + str(task_num) + '_v' + str(veh_type_num) + '_c' + str(cap_num) + label_name + '/'
mean_sample_num = 0
print('T', task_num, 'V', veh_type_num, 'C', cap_num, ',', veh_has_cap_num, 'S', sample_num_bound, ': ', end = '')
if not os.path.exists(test_folder + case_folder):
    os.makedirs(test_folder + case_folder)
for i_rand in range(rand_num):
    file_name = test_folder + case_folder + 'case' + str(i_rand) + '.yaml'
    while True:
        param_manager.rand_matrix(task_num, veh_type_num, cap_num, task_req_num = task_req_num, task_req_range = task_req_range, veh_cap_range = veh_cap_range, veh_has_cap_num = veh_has_cap_num)
        total_sample_num = param_manager.total_sample()
        if sample_num_bound[0] < total_sample_num < sample_num_bound[1]:
            break
    param_manager.save_to_file(file_name)
    print(param_manager.total_sample(), end='\t')
    mean_sample_num += total_sample_num
mean_sample_num /= rand_num
print('mean', mean_sample_num)

# Case 2-3
task_num = 20
veh_type_num = 6
cap_num = 32
veh_has_cap_num = 8
task_req_num = [1,1,2,2,3,3,4,4,5,5,  1,1,2,2,3,3,4,4,5,5]
sample_num_bound = [140000, 160000]
label_name = ''
case_folder = 'm' + str(task_num) + '_v' + str(veh_type_num) + '_c' + str(cap_num) + label_name + '/'
mean_sample_num = 0
print('T', task_num, 'V', veh_type_num, 'C', cap_num, ',', veh_has_cap_num, 'S', sample_num_bound, ': ', end = '')
if not os.path.exists(test_folder + case_folder):
    os.makedirs(test_folder + case_folder)
for i_rand in range(rand_num):
    file_name = test_folder + case_folder + 'case' + str(i_rand) + '.yaml'
    while True:
        param_manager.rand_matrix(task_num, veh_type_num, cap_num, task_req_num = task_req_num, task_req_range = task_req_range, veh_cap_range = veh_cap_range, veh_has_cap_num = veh_has_cap_num)
        total_sample_num = param_manager.total_sample()
        if sample_num_bound[0] < total_sample_num < sample_num_bound[1]:
            break
    param_manager.save_to_file(file_name)
    print(param_manager.total_sample(), end='\t')
    mean_sample_num += total_sample_num
mean_sample_num /= rand_num
print('mean', mean_sample_num)


# Case 3-1
task_num = 40
veh_type_num = 6
cap_num = 8
veh_has_cap_num = 2
task_req_num = [1,1,2,2,3,3,4,4,5,5,  1,1,2,2,3,3,4,4,5,5,  1,1,2,2,3,3,4,4,5,5,  1,1,2,2,3,3,4,4,5,5]
sample_num_bound = [280000, 320000]
label_name = ''
case_folder = 'm' + str(task_num) + '_v' + str(veh_type_num) + '_c' + str(cap_num) + label_name + '/'
mean_sample_num = 0
print('T', task_num, 'V', veh_type_num, 'C', cap_num, ',', veh_has_cap_num, 'S', sample_num_bound, ': ', end = '')
if not os.path.exists(test_folder + case_folder):
    os.makedirs(test_folder + case_folder)
for i_rand in range(rand_num):
    file_name = test_folder + case_folder + 'case' + str(i_rand) + '.yaml'
    while True:
        param_manager.rand_matrix(task_num, veh_type_num, cap_num, task_req_num = task_req_num, task_req_range = task_req_range, veh_cap_range = veh_cap_range, veh_has_cap_num = veh_has_cap_num)
        total_sample_num = param_manager.total_sample()
        if sample_num_bound[0] < total_sample_num < sample_num_bound[1]:
            break
    param_manager.save_to_file(file_name)
    print(param_manager.total_sample(), end='\t')
    mean_sample_num += total_sample_num
mean_sample_num /= rand_num
print('mean', mean_sample_num)

# # Case 3-2
task_num = 40
veh_type_num = 6
cap_num = 16
veh_has_cap_num = 4
task_req_num = [1,1,2,2,3,3,4,4,5,5,  1,1,2,2,3,3,4,4,5,5,  1,1,2,2,3,3,4,4,5,5,  1,1,2,2,3,3,4,4,5,5]
sample_num_bound = [280000, 320000]
label_name = ''
case_folder = 'm' + str(task_num) + '_v' + str(veh_type_num) + '_c' + str(cap_num) + label_name + '/'
mean_sample_num = 0
print('T', task_num, 'V', veh_type_num, 'C', cap_num, ',', veh_has_cap_num, 'S', sample_num_bound, ': ', end = '')
if not os.path.exists(test_folder + case_folder):
    os.makedirs(test_folder + case_folder)
for i_rand in range(rand_num):
    file_name = test_folder + case_folder + 'case' + str(i_rand) + '.yaml'
    while True:
        param_manager.rand_matrix(task_num, veh_type_num, cap_num, task_req_num = task_req_num, task_req_range = task_req_range, veh_cap_range = veh_cap_range, veh_has_cap_num = veh_has_cap_num)
        total_sample_num = param_manager.total_sample()
        if sample_num_bound[0] < total_sample_num < sample_num_bound[1]:
            break
    param_manager.save_to_file(file_name)
    print(param_manager.total_sample(), end='\t')
    mean_sample_num += total_sample_num
mean_sample_num /= rand_num
print('mean', mean_sample_num)

# Case 3-3
task_num = 40
veh_type_num = 6
cap_num = 32
veh_has_cap_num = 8
task_req_num = [1,1,2,2,3,3,4,4,5,5,  1,1,2,2,3,3,4,4,5,5,  1,1,2,2,3,3,4,4,5,5,  1,1,2,2,3,3,4,4,5,5]
sample_num_bound = [280000, 320000]
label_name = ''
case_folder = 'm' + str(task_num) + '_v' + str(veh_type_num) + '_c' + str(cap_num) + label_name + '/'
mean_sample_num = 0
print('T', task_num, 'V', veh_type_num, 'C', cap_num, ',', veh_has_cap_num, 'S', sample_num_bound, ': ', end = '')
if not os.path.exists(test_folder + case_folder):
    os.makedirs(test_folder + case_folder)
for i_rand in range(rand_num):
    file_name = test_folder + case_folder + 'case' + str(i_rand) + '.yaml'
    while True:
        param_manager.rand_matrix(task_num, veh_type_num, cap_num, task_req_num = task_req_num, task_req_range = task_req_range, veh_cap_range = veh_cap_range, veh_has_cap_num = veh_has_cap_num)
        total_sample_num = param_manager.total_sample()
        if sample_num_bound[0] < total_sample_num < sample_num_bound[1]:
            break
    param_manager.save_to_file(file_name)
    print(param_manager.total_sample(), end='\t')
    mean_sample_num += total_sample_num
mean_sample_num /= rand_num
print('mean', mean_sample_num)
