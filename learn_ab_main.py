import numpy as np
from ParamManager import ParamManager
from SimWrapper import SimWrapper
from ConstrLearner import ConstrLearner
import helper
import time

def learn_models(param_file = None, result_file = 'temp_result.yaml', sample_file = None):
    np.set_printoptions(precision=4)
    perform_thres = 0.5
    flag_noise = False
    noise_rate = 0.1

    param_manager = ParamManager()
    param_manager1 = ParamManager()
    if param_file:
        param_manager.read_from_file(param_file)
        param_manager1.read_from_file(param_file)
    if flag_noise:
        param_manager1.add_noise(noise_rate)
    sim_wrapper = SimWrapper(param_manager)

    sample_dict = None
    if sample_file:
        sample_dict = helper.read_dict(sample_file)

    # Generate training data
    veh_sample_train = []
    performance_sample_train = []
    veh_sample_all = []
    performance_sample_all = []
    for i_task in range(param_manager.task_num):
        # Prepare test data
        veh_arange_list = []
        for i_veh_type in param_manager.task_veh_relavance[i_task]:
            veh_arange_list.append(np.arange(param_manager.veh_num[i_veh_type]+1))

        veh_sample_list = np.meshgrid(*veh_arange_list)
        veh_sample_array = np.array(veh_sample_list).reshape(len(veh_sample_list), -1).T
        sample_num = veh_sample_array.shape[0]
        veh_sample = np.zeros((sample_num, param_manager.veh_type_num), dtype=np.int64)
        veh_sample[:, param_manager.task_veh_relavance[i_task]] = veh_sample_array

        performance_sample = np.zeros(sample_num, dtype=np.float64)
        for i_sample in range(sample_num):
            performance_sample[i_sample] = sim_wrapper.task_performance(i_task, veh_sample[i_sample])
        veh_sample_all.append(veh_sample)
        performance_sample_all.append(performance_sample)
        # print('veh_sample = ', veh_sample.shape)
        # print('performance_sample = ', performance_sample.shape)

        # Prepare training data
        X_observed_name = 'X_observed' + str(i_task)
        if sample_dict and (X_observed_name in sample_dict):
            veh_sample_temp = np.array(sample_dict[X_observed_name])
            sample_num_temp = veh_sample_temp.shape[0]
            performance_sample_temp = np.zeros(sample_num_temp, dtype=np.float64)
            for i_sample in range(sample_num_temp):
                performance_sample_temp[i_sample] = sim_wrapper.task_performance(i_task, veh_sample_temp[i_sample])
            temp_unique = np.unique(veh_sample_temp, axis=0)
            print('Total unique samples:', temp_unique.shape)
            temp_unique = np.unique(veh_sample_temp[:100], axis=0)
            print('Total unique samples:', temp_unique.shape)
        else:
            veh_sample_temp = veh_sample
            performance_sample_temp = performance_sample
        veh_sample_train.append(veh_sample_temp)
        performance_sample_train.append(performance_sample_temp)

    result_dict = {}

    # Our learner: linear program
    print('Ours')
    lin_start_time = time.time()
    constr_learner = ConstrLearner(param_manager1)
    lin_veh_cap_mat, lin_task_req_mat, lin_veh_cap_mat_ori, lin_task_req_mat_ori = constr_learner.optimize(veh_sample_train, performance_sample_train)
    lin_time = time.time() - lin_start_time
    print('Train result')
    lin_label_pred_train = helper.predict(veh_sample_train, lin_veh_cap_mat, lin_task_req_mat)
    lin_train_table = helper.evaluate_result(veh_sample_train, performance_sample_train, perform_thres, lin_label_pred_train)
    print('Test result')
    lin_label_pred_all = helper.predict(veh_sample_all, lin_veh_cap_mat, lin_task_req_mat)
    lin_result_table = helper.evaluate_result(veh_sample_all, performance_sample_all, perform_thres, lin_label_pred_all)
    print('A = ', lin_veh_cap_mat)
    print('B = ', lin_task_req_mat)
    print('A_ori = ', lin_veh_cap_mat_ori)
    print('B_ori = ', lin_task_req_mat_ori)
    print('time = ', lin_time)

    # Show ground truth
    print('\nGround truth')
    print("gt_veh_cap_mat = ", param_manager.gt_veh_cap_mat)
    print("gt_task_req_mat = ", param_manager.gt_task_req_mat)
    print('\n')

    # Generate result dict
    result_dict['lin_veh_cap_mat'] = lin_veh_cap_mat.tolist()
    result_dict['lin_task_req_mat'] = lin_task_req_mat.tolist()
    result_dict['lin_veh_cap_mat_ori'] = lin_veh_cap_mat_ori.tolist()
    result_dict['lin_task_req_mat_ori'] = lin_task_req_mat_ori.tolist()
    result_dict['lin_time'] = lin_time
    result_dict['lin_result_table'] = lin_result_table
    result_dict['lin_train_table'] = lin_train_table

    result_dict['gt_veh_cap_mat'] = param_manager.gt_veh_cap_mat.tolist()
    result_dict['gt_task_req_mat'] = param_manager.gt_task_req_mat.tolist()
    result_dict['param'] = param_manager.to_dict()

    helper.save_dict(result_file, result_dict)

if __name__ == '__main__':
    learn_models()
