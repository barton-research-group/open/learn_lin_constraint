import numpy as np
import gurobipy as gp
from gurobipy import GRB
from ParamManager import ParamManager

class ConstrLearner:
    def __init__(self, param_manager: ParamManager):
        self.param_manager = param_manager
        self.solver_time_limit = 300000
        self.flag_relax_constr = False
        self.alpha_learn = 500.0
        self.alpha_a = 0.25
        self.alpha_b = 1.0
    
    def optimize(self, veh_sample, performance_sample):
        veh_cap_mat = np.zeros((self.param_manager.veh_type_num, self.param_manager.cap_num), dtype=np.float64)
        task_req_mat = np.zeros((self.param_manager.task_num, self.param_manager.cap_num), dtype=np.float64)

        env = gp.Env(empty=True)
        env.setParam('OutputFlag', 0)
        env.start()

        performance_thres = 0.5 # TODO: change this hard-coded value
        for i_cap in range(self.param_manager.cap_num):
            solver = gp.Model("Routing", env=env)
            a_var = solver.addVars(self.param_manager.veh_type_num, vtype=GRB.CONTINUOUS, name='a', lb=0.0, ub=1.0)
            b_var = solver.addVars(self.param_manager.task_num,     vtype=GRB.CONTINUOUS, name='b', lb=0.0, ub=100.0)
            a_min_var = solver.addVar(0.0, 1.0, 0.0, GRB.CONTINUOUS, "a_min")
            if self.flag_relax_constr:
                xi_var = []

            solver.Params.timeLimit = self.solver_time_limit

            # Objective function
            obj = 0.0
            obj += self.alpha_a * a_min_var # TODO: This 1.0 is a hard-coded weight to trade off the A and b, the larger, the more aggressive
            for i_task in range(self.param_manager.task_num):
                obj += b_var[i_task] * (self.alpha_b / self.param_manager.task_num)
            # Set unrelavant requirements to zero
            for i_task in range(self.param_manager.task_num):
                if not self.param_manager.task_req_bool[i_task, i_cap]:
                    b_var[i_task].ub = 0.0
            # Set unrelavant capabilities to zero
            for i_veh_type in range(self.param_manager.veh_type_num):
                if not self.param_manager.veh_cap_bool[i_veh_type, i_cap]:
                    a_var[i_veh_type].ub = 0.0
            # avar min
            for i_veh_type in range(self.param_manager.veh_type_num):
                if self.param_manager.veh_cap_bool[i_veh_type, i_cap]:
                    constr_name = 'min_A[' + str(i_veh_type) + ']'
                    solver.addConstr(a_var[i_veh_type] >= a_min_var, constr_name)
            # Normalize the a variables
            constr = 0
            for i_veh_type in range(self.param_manager.veh_type_num):
                if self.param_manager.veh_cap_bool[i_veh_type, i_cap]:
                    constr += a_var[i_veh_type]
            constr_name = 'normalize_A'
            solver.addConstr(constr == 1, constr_name)
            # Learning
            for i_task in range(self.param_manager.task_num):
                sample_num = performance_sample[i_task].shape[0]
                if self.flag_relax_constr:
                    xi_var_temp = solver.addVars(sample_num, vtype=GRB.CONTINUOUS, name='xi'+str(i_task), lb=0.0, ub=10.0)
                    xi_var.append(xi_var_temp)
                for i_sample in range(sample_num):
                    if performance_sample[i_task][i_sample] < performance_thres:
                        continue
                    constr = 0
                    for i_veh_type in range(self.param_manager.veh_type_num):
                        constr += veh_sample[i_task][i_sample, i_veh_type] * a_var[i_veh_type]
                    constr_name = 'learn[' + str(i_task) + ', ' + str(i_sample) + ']'
                    if not self.flag_relax_constr:
                        solver.addConstr(constr >= b_var[i_task], constr_name)
                    else:
                        # Optimization form
                        solver.addConstr(constr >= b_var[i_task] - xi_var[i_task][i_sample], constr_name)
                        obj -= xi_var[i_task][i_sample] * (self.alpha_learn / (self.param_manager.task_num * sample_num))
            solver.setObjective(obj, GRB.MAXIMIZE) # MINIMIZE, MINIMIZE
            solver.optimize()
            for i_veh_type in range(self.param_manager.veh_type_num):
                veh_cap_mat[i_veh_type, i_cap] = a_var[i_veh_type].x
                # print(a_var[i_veh_type].varname, a_var[i_veh_type].x)
            for i_task in range(self.param_manager.task_num):
                task_req_mat[i_task, i_cap] = b_var[i_task].x
                # print(b_var[i_task].varname, b_var[i_task].x)

        veh_cap_mat_ori = veh_cap_mat + 0.0
        task_req_mat_ori = task_req_mat + 0.0

        self.param_manager.normalize_model(veh_cap_mat, task_req_mat)
        # for i_cap in range(self.param_manager.cap_num):
        #     cap_norm = veh_cap_mat[:, i_cap]
        #     cap_norm = cap_norm[cap_norm > self.param_manager.cap_eps]
        #     if cap_norm.shape[0] == 0:
        #         continue
        #     cap_norm = cap_norm.min()
        #     # print('cap_norm = ', cap_norm)
        #     veh_cap_mat[:, i_cap] /= cap_norm
        #     task_req_mat[:, i_cap] /= cap_norm
        return veh_cap_mat, task_req_mat, veh_cap_mat_ori, task_req_mat_ori






