import os
from learn_ab_main import learn_models
from helper import gen_samples
# os.environ['KMP_DUPLICATE_LIB_OK']='True'

if __name__ == '__main__':
    flag_random = True

    repeat_num = 10
    test_folder = 'test_case/temp_case/'
    result_folder = 'test_result/temp_rand/'
    case_folder_list = ['m8_v6_c8_sparse', 'm8_v6_c8', 'm8_v6_c16', 'm8_v6_c32', 'm20_v6_c8', 'm40_v6_c8', 'm40_v6_c16', 'm40_v6_c32']

    if flag_random:
        for i_case in range(len(case_folder_list)):
            case_folder = case_folder_list[i_case] + '/'
            # Make result folder
            if not os.path.exists(result_folder + case_folder):
                os.makedirs(result_folder + case_folder)
            for i_repeat in range(repeat_num):
                param_file = test_folder + case_folder + 'case' + str(i_repeat) + '.yaml'
                sample_file = result_folder + case_folder + 'sample' + str(i_repeat) + '.yaml'
                gen_samples(param_file = param_file, sample_file=sample_file)

    for i_case in range(len(case_folder_list)):
        case_folder = case_folder_list[i_case] + '/'
        # Make result folder
        if not os.path.exists(result_folder + case_folder):
            os.makedirs(result_folder + case_folder)
        for i_repeat in range(repeat_num):
            param_file = test_folder + case_folder + 'case' + str(i_repeat) + '.yaml'
            sample_file = result_folder + case_folder + 'sample' + str(i_repeat) + '.yaml'
            result_file = result_folder + case_folder + 'case' + str(i_repeat) + '.yaml'
            print('param_file = ', param_file)
            print('result_file = ', result_file)
            # Learn
            if flag_random:
                learn_models(param_file, result_file, sample_file)
            else:
                learn_models(param_file, result_file)
