import numpy as np
from ParamManager import ParamManager

class SimWrapper:
    def __init__(self, param_manager: ParamManager):
        self.param_manager = param_manager
        self.cap_eps = 1e-3
    
    def task_performance(self, i_task, veh_vec):
        performance = self.param_manager.gt_veh_cap_mat.T.dot(veh_vec) - self.param_manager.gt_task_req_mat[i_task] # (cap_num, veh_num) * (veh_num,) = (cap_num,)
        performance = (performance >= -self.cap_eps).all() + 0.0
        return performance

    def task_performance_energy(self, i_task, veh_vec):
        performance = self.param_manager.gt_veh_cap_mat.T.dot(veh_vec) - self.param_manager.gt_task_req_mat[i_task] # (cap_num, veh_num) * (veh_num,) = (cap_num,)
        # performance = (performance >= -self.cap_eps).all() + 0.0
        # energy = veh_vec.sum() + 0.0
        # objective = 2.0 * performance - 0.03 * energy
        # objective = - (2.0 * np.abs(performance - 1.0) + 0.03 * energy)

        # 2:
        performance1 = self.param_manager.gt_veh_cap_mat.T.dot(veh_vec) - self.param_manager.gt_task_req_mat[i_task] # (cap_num, veh_num) * (veh_num,) = (cap_num,)
        performance = (performance1 >= -self.cap_eps).all() + 0.0
        energy = veh_vec.sum() + 0.0

        weight = np.ones_like(performance1)
        weight[performance1 < 0] = -10.0
        weight[performance1 > 0] = 0.5
        objective = 3.0 * performance - ( 0.15 * weight.dot(performance1) / performance1.shape[0] ) - 3.0

        return performance, energy, objective
